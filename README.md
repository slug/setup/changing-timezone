# How to Change Timezone on Ubuntu/Linux Mint (from Command Line)

Open a terminal.

To view the current timezone (and other time-related information),

    timedatectl

To view the list of all available timezones,

    timedatectl list-timezones

To change the timezone,

    sudo timedatectl set-timezone _timezone name_





